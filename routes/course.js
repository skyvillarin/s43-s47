//Dependencies and Modules

const express = require("express");
const courseController = require("../controllers/course");
const auth = require("../auth");
const {verify, verifyAdmin} = auth; //destructure process

//Routing Component
const router = express.Router();

//ACTIVITY: Admin Course Creation
router.post("/", verify, verifyAdmin, courseController.addCourse);

//route for retrieving all courses
router.get("/all", courseController.getAllCourses);

//create a route for getting all active courses - activity
// use default endpooint
router.get("/", courseController.getAllActiveCourses);

//get a specific course
router.get("/:courseId", courseController.getCourse);

//edit a specific course
router.put("/:courseId",verify, verifyAdmin, courseController.updateCourse);


//group activity: archive route
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

//group activity: create a route for activating a course using PUT method. Restores courses that have been deactivated. 
router.put("/:courseId/activate",verify, verifyAdmin, courseController.activateCourse);

//Export Route System
module.exports = router;