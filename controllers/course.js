const Course = require("../models/Course");

// ACTIVITY: Add Course Controller
module.exports.addCourse = (req, res) => {
  const newCourse = new Course({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  });

  return newCourse.save().then((course, error) => {
      if (error) {
        return res.send(false);
      } else {
        console.error(newCourse);
        return res.send(true);
      }
    })
    .catch((err) => res.send(err));
};

// Retrieve all courses
/*
  Retrieve all the courses from the database
*/

//We will use the find() method for our course model
module.exports.getAllCourses = (req,res)=>{
  return Course.find({}).then(result=>{
    return res.send(result)
  })
}

//getAllActiveCourses - activity
module.exports.getAllActiveCourses = (req,res)=>{
  return Course.find({isActive:true}).then(result=>{
    return res.send(result)
  })
}

//Get a specific course
module.exports.getCourse = (req,res)=>{
  return Course.findById(req.params.courseId).then(result=>{
    return res.send(result)
  })
}

//edit a specific course
module.exports.updateCourse = (req,res)=>{

  let updatedCourse = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  }

  return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error)=>{
    if(error){
      return res.send(false);
    }else{
      return res.send(true);
    }
  })
}


//group activity: archive controller
module.exports.archiveCourse = (req,res) => {

  let updatedIsActive = {
    isActive : false
  }

  return Course.findByIdAndUpdate(req.params.courseId, updatedIsActive).then((course,error)=> {
    if (error) {
      return res.send(false);
    }else {
      res.send(true);
    }
  })

}

// group activity: create a controller method "activateCourse" for activating a course obtaining the course ID from the request params. Simply update the course isActive status into true


// module.exports.activateCourse = (req, res) => {
//   const courseId = req.params.courseId;

//   return Course.findByIdAndUpdate(courseId, { isActive: true }).then(
//     (course, error) => {
//       if (error) {
//         return res.send(false);
//       } else {
//         return res.send(true);
//       }
//     }
//   );
// };

module.exports.activateCourse = (req, res) => {
  return Course.findByIdAndUpdate(req.params.courseId)
    .then(savedCourse => {
      if (!savedCourse) {
        return res.send(false); 
      }
      if (savedCourse.isActive) {
        return res.send("Course already active"); 
      }
      savedCourse.isActive = true;
      savedCourse.save()
        .then(() => {
          return res.send(true); 
        })
        .catch(saveError => {
          console.error(saveError);
          return res.send(false); 
        });
    })
    .catch(error => {
      console.error(error);
      return res.send(false); 
    });
};












  
    


 








