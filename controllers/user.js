//controller
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");

//Check if the email exists already
/*
	Steps:
	1. "find" mongoose method to find duplicate items
	2. "then" method to send a response back to the FE application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqbody) =>{

	return User.find({email:reqbody.email}).then(result=>{

		//find method returns a record if a match is found
		if(result.length > 0){
			return true;
		}
		//no duplicate found
		//the user is not yet registered in the db
		else{
			return false;
		}


	})
}

//User Registration
/*
	Steps:
	1. create a new user object
	2. make sure that the password is encrypted
	3. save the new User to the database
*/

module.exports.registerUser = (reqbody) =>{

	let newUser = new User({
		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email:reqbody.email,
		mobileNo:reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password,10)
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false;
		}else{
			return true
		}

	})
	.catch(err=>err)
}

//router,post("/register",userController.registerUser);

//User authentication
/*
Steps: 
1. Check the db if user email exists
2. Compare the password from req.body and email from db
3. Generate a JWT if the user logged in and return false if not
*/

module.exports.loginUser = (req,res)=>{
	return User.findOne({email:req.body.email}).then(result=>{
		if(result === null){
			return false;
		}else{
			//compareSync method is used to compare a non-encrypted password and from the encrypted password from the db
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)
			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}
			//else password does not match
			else{
				return res.send(false);
			}
		}
	})
	.catch(err=>res.send(err))
}

// ACTIVITY
// Create a getProfile controller method for retrieving the details of the user:
/* 
Find the document in the database using the user's ID
Reassign the password of the returned document to an empty string
Return the result back to the frontend
*/
/*module.exports.getProfile = (req, res) => {
  User.findById(req.body.id)
    .then((user) => {
      if (!user) {
        return res.send("No such user found.");
      }
      user.password = "";
      return res.send(user);
    })
    .catch((err) => {
      console.error(err);
      return res.send(err);
    });
};*/

module.exports.getProfile=(req,res) =>{
	return User.findById(req.user.id).then(result=>{
		result.password = "";
		return res.send(result);
	})
	.catch(err=>res.send(err))
};


//Enroll a user to a class
/*
Find the document in the database using the user's ID
Add the course ID to the user's enrollment array
Update the document in the MongoDB Atlas Database
*/
module.exports.enroll = async (req, res) => {

	// to check the user id and the courseId
	console.log(req.user.id); 
	console.log(req.body.courseId);

	// checks if user is admin and denies the enrollment
	if(req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	// creates "isUserUpdated" variable and returns true upon successful update, otherwise returns error
	let isUserUpdated = await User.findById(req.user.id).then(user => {
		
		// adds the courseId in an object and push that object into the user's "enrollment" array
		let newEnrollment = {
			courseId: req.body.courseId
		}

		// add the course in the "enrollments" array
		user.enrollments.push(newEnrollment);

		// save the updated user and return true if successful or the error message if failed.
		return user.save().then(user => true).catch(err => err.message);

	});

	// Checks if there are error in updating the user
	if (isUserUpdated !== true){
		return res.send({ message: isUserUpdated});
	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		let enrollee = {
			userId: req.user.id
		}
		course.enrollees.push(enrollee);
		return course.save().then(course => true).catch(err => err.message);
	})

	// Checks if there are error in updating the course
	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated});
	}

	// Checks if both user updaet and course update are successful
	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully."})
	}
}


















